Global objects

Class: Buffer

__dirname
__filename

clearImmediate(immediateObject)
clearInterval(intervalObject)
clearTimeout(timeoutObject)

queueMicrotask(callback)
setImmediate(callback[, ...args])
setInterval(callback, delay[, ...args])
setTimeout(callback, delay[, ...args])
process.nextTick()


console

global //window in browser

x = 123
global.x =  123
var x =  123
let x =  123
const x =  123

module
exports
exports = {x:234}
module.exports.test = 123
var imports = require('./somemodule.js')
imports

process

require()


TextDecoder
TextEncoder
URL
URLSearchParams

WebAssembly