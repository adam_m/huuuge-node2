
export interface CouponQueryParams {
    filter?: string,
    limit?: string,
    offset?: string
  }

export interface CouponDTO {
    code: string;
    discount: number;
}

export interface CouponsListResponse {
    items: CouponDTO[];
    filter?: string;
    limit?: number;
    offset?: number;
}