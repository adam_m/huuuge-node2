import http from "http";
import express from 'express'
import { userRoutes } from "./routes/users.routes";
import routes from "./routes";

const app = express()

app.get('/', (req, res) => {
  res.send('<h1>Hello Express</h1>');
});

app.use('/', routes);


const HOST = process.env.HOST || '127.0.0.1'
const PORT = process.env.PORT || '3000'

app.listen(parseInt(PORT), HOST, () => {
  console.log(`Listening on ${HOST} port ${PORT}`)
})
