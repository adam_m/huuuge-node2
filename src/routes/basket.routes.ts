import { Router } from 'express';

export const basketRoutes = Router();

basketRoutes.get('/remove/:itemId', (req, res) => {
    // remove from basket

    res.send(`Item ${req.params['itemId']} removed`);
});