
import { Router } from 'express';
import { CouponQueryParams, CouponsListResponse , CouponDTO} from '../interfaces/coupons.interfaces';


export const couponsRoutes = Router()

couponsRoutes.get<{}, CouponsListResponse, null, CouponQueryParams>('/', (req, res) => {
  const { filter } = req.query
  const limit = parseInt(req.query.limit || '10')
  const offset = parseInt(req.query.offset || '0')

const response = {
  items: codesList,
  filter,
  limit, 
  offset
}

  res.send(response)
})

couponsRoutes.get<{ code: string }, CouponDTO>("/:code", function (req, res) {
  codesList.filter(item => item.code === req.params['code']);
  if(codesList.length === 0){
    res.sendStatus(404);
  }

    res.send(codesList[0]);
});


const codesList: CouponDTO[] = [
  {
      code : "dskhlkwejsglks",
      discount: 0.15
  },
  {
      code : "454ge54te",
      discount: 0.1
  },
];